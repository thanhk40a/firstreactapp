import { template } from '@babel/core';

import * as ActionType from '../constanst/actiontype'

/**
 * Add Cat function with action
 * @param {*} state 
 * @param {*} action 
 */
 const addCat = (state,action) => ([
    ...state,
    //add qty var to check if cats add the same
    {...action.payload,qty: 1}
  ]
 )


 /**
  * check reduce with action type
  * @type : ADD
  * @type : REMOVE
  * @param {*} state 
  * @param {*} action 
  */
function catReducer(state = [],action){
    switch (action.type) {
        // action type = add
        case ActionType.CAT_ADD: {
          if(state.some((item,id) => item.cat.id === action.payload.cat.id)){
            return state.map(item => (item.cat.id === action.payload.cat.id ? { ...item, qty: item.qty + 1 } : item));
          } else {
            return addCat(state,action);
          }
        }
        // action type = remove
        case ActionType.CAT_REMOVE : {
          return state.filter((item, id) => item.cat.id !== action.payload.cats.cat.id);
        }
        // action type = update
        case ActionType.CAT_UPDATE : {
          if(state.some((item,id) => item.cat.id === action.payload.cat.id)){
            return state.map(item => (item.cat.id === action.payload.cat.id) ? {...item, qty: action.qtyCurr} : item)
          }
        }
        default:
          return state;
      }
}

export default catReducer;