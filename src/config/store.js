import { createStore, combineReducers } from 'redux'

import rootReducer from '../reducers/index.js'

/**
 * Create Store redux on reducers
 */
const store = createStore(
    rootReducer,
    // call a function if browser had redux dev tools
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store;