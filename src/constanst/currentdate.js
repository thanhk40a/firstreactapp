
/**
 * function get current date,month,year 
 * @param {*} separator 
 */
export function getCurrentDate(separator=''){

    let newDate = new Date()
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    
    return `${year}${separator}${month<10?`0${month}`:`${month}`}${separator}${date}`
}

/**
 * Get date
 * @param {*} separator 
 */
export function getDate(separator=''){
    let newDate = new Date()
    let date = newDate.getDate();
    return date;
}

/**
 * Get month
 * @param {*} separator 
 */
export function getMonth(separator=''){
    let newDate = new Date();
    let month = newDate.getMonth() + 1;
    return month;
}

/**
 * Get year
 * @param {*} separator 
 */
export function getYear(separator=''){
    let newDate = new Date();
    let year = newDate.getFullYear();
    return year;
}