
//Landing router
export const Landing = '/';
//Home router
export const Home = '/home';
//About router
export const About = '/about';
//Login router
export const Login = '/login'
//Register router
export const Register = '/sign-up'
//Cat Cart router
export const CatsCart = '/cats-cart'
//Cat Detail router
export const CatsDetail = '/cats-detail/'