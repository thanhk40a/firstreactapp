
//add cat type
export const CAT_ADD = "CAT_ADD"

//remove cat type
export const CAT_REMOVE = "CAT_REMOVE"

//remove all cat type
export const CAT_REMOVE_ALL = "CAT_REMOVE_ALL"

// update cat type
export const CAT_UPDATE = "CAT_UPDATE"