import React from 'react';
import ReactDOM from 'react-dom';
import Firebase, { FirebaseContext } from './components/Firebase';
import {Provider} from 'react-redux'

import './index.css';
import App from './components/App'
import * as serviceWorker from './serviceWorker';
import 'bootstrap-css-only/css/bootstrap.min.css';
import './assets/css/base.css'
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './assets/css/loading.css'
import './assets/css/hoverstyle.css'
import store from'./config/store'

/**
 * Call app page from components
 */
ReactDOM.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>
            <App />
        </FirebaseContext.Provider>
    </Provider>,
document.getElementById('root'));

serviceWorker.unregister();
