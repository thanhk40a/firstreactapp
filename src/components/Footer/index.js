import React from 'react'

/**
 * test CSS on reactjs
 */
const styleFooter = {
    textAlign : 'center'
}

/**
 * Footer Page
 * return ...
 */
const Footer = () => (
    <div>
        <hr/>
        <footer>
            <p style={styleFooter}>Copyright <i className="fas fa-copyright"></i> Thành</p>
        </footer>
    </div>
);

export default Footer;
