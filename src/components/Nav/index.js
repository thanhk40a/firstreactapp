import React from 'react'
import { Link } from 'react-router-dom'
import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, MDBRow, MDBCol } from 'mdbreact';

import cats_header_icon from '../../assets/images/cats_header_icon.svg'
import * as Route from '../../constanst/router'
import {getCurrentDate,getDate,getYear,getMonth} from '../../constanst/currentdate'
import SignOut from '../Login/signout'
import { withFirebase } from '../Firebase';
import Firebase from '../Firebase'

/**
 * Nav Page
 * @param {*} authUser
 */
class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
        };
        this.onClick = this.onClick.bind(this);
    }
  
    onClick() {
      this.setState({
          collapse: !this.state.collapse,
        });
    }
  
    render() {
        const bgPink = {backgroundColor: '#e91e63'}
        const total = this.props.catStage.reduce((result, cats) => result + cats.qty, 0)
        return(
                <>
                 {/* Header of website */}
                <MDBContainer fluid className="firstnav">
                    <MDBRow>
                        <MDBCol size="8">
                            <h1>Cems clone</h1>
                        </MDBCol>
                        <MDBCol size="4" className="text-right">
                        <p> ngày {getDate()} tháng {getMonth()} năm {getYear()} </p>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>

                {/* Navbar */}
                <MDBNavbar fixed="top" style={bgPink} dark expand="md" scrolling>
                    <MDBContainer>
                        <Link className="navbar-brand" to={Route.Landing}> 
                            <img src={cats_header_icon} style={{height: "34px"}}/> CatsLand
                        </Link>
                        <MDBNavbarToggler onClick={ this.onClick } />
                        <MDBCollapse isOpen = { this.state.collapse } navbar>
                        <MDBNavbarNav left>
                        </MDBNavbarNav>
                        <MDBNavbarNav right>
                            <MDBNavItem>
                                <Link className="nav-link" to={Route.Landing}><i className="fas fa-cat"></i>Home</Link>
                            </MDBNavItem>
                            <MDBNavItem>
                                <Link className="nav-link hvr-icon-pulse-grow" to={Route.About}><i className="far fa-angry"></i>About</Link>
                            </MDBNavItem>
                            <MDBNavItem>
                                <Link to={Route.CatsCart} className = "nav-link hvr-icon-pulse-grow">Cats : {total}</Link>
                            </MDBNavItem>
                            {this.props.authUser ? <NavWithAuth email = {this.props.authUser.email} /> : <NavWithoutAuth/>}
                        </MDBNavbarNav>
                        </MDBCollapse>
                    </MDBContainer>
                </MDBNavbar>
                </>
        );
    }
  }

/**
 * if user is not logged
 */
const NavWithoutAuth = () => (
    <>
       <MDBNavItem>
            <Link className="nav-link hvr-icon-pulse-grow" to={Route.Login}><i className="fas fa-sign-in-alt"></i>Login</Link>
        </MDBNavItem>
        <MDBNavItem>
            <Link className="nav-link hvr-icon-pulse-grow" to={Route.Register}>Register</Link>
        </MDBNavItem>
    </>
)

/**
 * if user is logged
 */
const NavWithAuth = ({email}) => (
    <>
        <MDBNavItem>
            <Link className="nav-link hvr-icon-pulse-grow" to=''><i className="far fa-user"></i>{email.substring(0,email.length-10)}</Link>
        </MDBNavItem>
        <MDBNavItem>
            <SignOut />
        </MDBNavItem>
    </>
)

export default withFirebase(Nav);
