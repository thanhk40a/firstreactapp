import React from 'react'
import {Link} from 'react-router-dom'

import * as ConstRoute from '../../constanst/router'
import {withFirebase} from '../Firebase'

/**
 * Sign user out of website
 * @param {*} firebase 
 */
const SignOut = ({firebase}) => (
    <Link className="nav-link" to={ConstRoute.Landing} onClick = {firebase.doSignOut}>
        <i className="fas fa-sign-out-alt"></i>Đăng Xuất
    </Link>
)

export default withFirebase(SignOut);