import React from 'react'
import { Redirect,Link,withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";
import { tsImportEqualsDeclaration } from '@babel/types';

import { withFirebase } from '../Firebase';
import * as ConstRoute from '../../constanst/router'

const INIT_SIGNIN = {
    email : '',
    password : '',
    error: null,
    loading : false
}

/**
 * Login form base with testing form in reactjs
 * return ...
 */
class LoginFormBase extends React.Component {
    constructor(props){
        super(props);
        this.state = {...INIT_SIGNIN};
    }

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    onSubmit = event => {
        const { email, password } = this.state;
         // fake authentication Promise
        this.props.firebase
        .doSignInWithEmailAndPassword(email, password)
        .then(() => {
            alert('Đăng Nhập Thành Công!');
            this.setState({ ...INIT_SIGNIN ,loading:true});
            this.props.history.push(ConstRoute.Landing);
        })
        .catch(error => {
            this.setState({ error });
        });
    
        event.preventDefault();
    };

    render() {
        const { email, password, error } = this.state;

        return (
           <MDBContainer style={{paddingTop: "2%"}}>
                <MDBRow>
                <MDBCol md="3">
                </MDBCol>
                <MDBCol md="6" className="card-login-col">
                    <MDBCard className="card-login">
                    <MDBCardBody>
                        <MDBCardHeader className="form-header deep-blue-gradient rounded">
                        <h3 className="my-3 text-center"> <MDBIcon icon="lock" /> Đăng Nhập </h3>
                        </MDBCardHeader>
                        <form onSubmit={this.onSubmit} >
                        <div className="grey-text">
                            <MDBInput label="Nhập email pls" name="email" icon="user" required group type="email" validate error="wrong" onChange = {this.onChange}  success="right" />
                            <MDBInput label="Nhập password pls" name="password" icon="lock" minLength = "6" onChange={this.onChange} group type="password" validate />
                        </div>
        
                        <div className="text-center mt-4">
                        <MDBBtn color="light-blue" className="mb-3" type="submit">
                            Đăng Nhập
                        </MDBBtn>
                        </div>
                        </form>
                        {error && <code>{error.message}</code>}
                        <MDBModalFooter className="mx-5 pt-3 mb-1">
                            <p className="font-small grey-text d-flex justify-content-end">
                                Không pải thành viên ?
                                <Link to={ConstRoute.Register} className="blue-text ml-1">
                                Đăng kí
                                </Link>
                            </p>
                        </MDBModalFooter>
                    </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
}

/**
 * compose login page with 2 component (withrouter and withfirebase)
 */
const LoginForm = compose(
    withRouter,
    withFirebase,
)(LoginFormBase);

/**
 * Login page
 */
const LoginPage = () => (
    <div>
        <LoginForm />
    </div>
);

export default LoginPage;

