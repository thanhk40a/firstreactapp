import React from 'react'
import {BrowserRouter as Router, Route, Link, Switch, Redirect} from "react-router-dom";
import {connect} from 'react-redux'

import Nav from '../Nav'
import About from '../About'
import Footer from '../Footer'
import Landing from '../Landing'
import ErrorPage from '../ErrorPage'
import LoginPage from '../Login'
import RegisterPage from '../Register'
import {withFirebase} from '../Firebase';
import * as RouteConst from '../../constanst/router'
import CatsCartPage from '../CatsCart'
/**
 * Main App Page
 * return ...
 */
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            authUser: null
        }
    }

    // fake authentication Promise
    authenticate() {
        return new Promise(resolve => setTimeout(resolve, 1000))
    }

    componentDidMount() {
        // check if user is logged
        this.listener = this
            .props
            .firebase
            .auth
            .onAuthStateChanged(authUser => {
                authUser
                    ? this.setState({authUser})
                    : this.setState({authUser: null});
            });

        // check if component ready to deploy
        this
            .authenticate()
            .then(() => {
                const ele = document.getElementById('ipl-progress-indicator')
                if (ele) {
                    // fade out
                    ele
                        .classList
                        .add('available')
                    setTimeout(() => {
                        // remove from DOM
                        ele.outerHTML = ''
                    }, 2000)
                }
            })
    }

    componentWillUnmount() {
        // avoid crashing when loading too much data from firebase....
        this.listener();
    }

    render() {
        return (
            <Router>
                {/* call nav bar from Nav */}
                <Nav authUser={this.state.authUser} catStage={this.props.catState}/>
                <Switch>
                    <Route exact path={RouteConst.Landing} component={Landing}/>
                    <Route path={RouteConst.About} component={About}/>
                    <Route path={RouteConst.Login} component={LoginPage}/>
                    <Route path={RouteConst.Register} component={RegisterPage}/>
                    <Route
                        path={RouteConst.CatsCart}
                        component={() => <CatsCartPage catStage={this.props.catState}/>
                        }
                    />
                    <Route component={ErrorPage}/>
                </Switch>
                <Footer/>
            </Router>
        )
    }
}

function mapStateToProps(state) {
    return {catState: state.catState}
}

export default withFirebase(connect(mapStateToProps)(App));
