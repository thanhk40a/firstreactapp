import React from 'react'

/**
 * Error page
 * @param {*} location 
 */
const ErrorPage = ({location}) => (
   <div>
        <h1>404 NOT FOUND</h1>
        <p>no match for <code>{location.pathname}</code></p>
   </div>
);

export default ErrorPage;
