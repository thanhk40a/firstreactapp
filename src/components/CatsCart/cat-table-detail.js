import React from 'react'
import {Link} from 'react-router-dom'

/**
 * Cat detail cart
 * @param {*} cats 
 * @param {*} catStage 
 * @param {*} removeCat 
 * @param {*} updateCat 
 */
export default function catDetail(cats,catStage,removeCat,updateCat){
    let valueCatAdd = 0;
    catStage.map((catStages,index)=>{
        if(catStages.cat.id === cats.cat.id){
            valueCatAdd = catStages.qty;
        }
    })
    return (
        <tr>
                <td width="8%">
                    <Link to="">
                        <img style={{maxWidth:"200px",maxHeight:"138px"}} src={cats.cat.image} width = "" heigth/>
                    </Link>
                </td>
                <td>
                <div className="row">
                    <p className="cart-cats-name">{cats.cat.name}</p>
                </div>
                <div className="row">
                    <input className="" type="number" defaultValue={cats.qty} min = "1" max = {cats.cat.quantity} name={cats.cat.id} onChange={(e) => {let cat = cats.cat;updateCat({cat},parseInt(e.target.value));}} />
                </div>
                 </td>
                    <td width="40%" align="right">
                    <div>
                        <p className="p-price-cart"> 
                            {valueCatAdd*cats.cat.price} <i className="fas fa-dollar-sign"></i>
                        </p>
                    </div>                       
                    <div className="row justify-content-center">
                        <button onClick={()=>removeCat({cats})} className="col-md-5 btn btn-danger">Delete</button>
                    </div>
                </td>
        </tr>
    )
}