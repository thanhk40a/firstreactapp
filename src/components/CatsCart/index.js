import React, {Component} from 'react'
import {connect} from 'react-redux'
import CatsTable from './cats-cart-table'
import {catsTableTotal} from './cats-cart-table'
import * as ActionType from '../../constanst/actiontype'
/**
 * Cart Page of Cats
 * @return ...
 */
function CatsCartPage (props) {
    return (
        <div
            className="container"
            style={{
                paddingTop: "3%"
            }}>
            <div className="row alert alert-dark">
                {/* Header */}
                <strong className="col-md-auto">Cat in Town</strong>
            </div>
            <div class="row">
                {/* Table of Cats Order */}
                <div className="col-md-8 box-list-cat">
                    {CatsTable(props.catStage,props.removeCat,props.updateCat)}
                </div>
                {/* Total Cats Order */}
                <div className="col-md-4 box-list-cat-total">
                    {catsTableTotal(props.catStage)}
                </div>
            </div>
        </div>
    )
}

/**
 * Dispatch to Props
 * @param {*} dispatch 
 */
function mapDispatchToProps(dispatch){
    return {
       addCat : (item) => {
            dispatch({ type: ActionType.CAT_ADD , payload : item})
       },
       removeCat : (item) => {
            dispatch({ type: ActionType.CAT_REMOVE , payload : item})
       },
       removeAllCat : (item) => {
            dispatch({ type: ActionType.CAT_REMOVE_ALL , payload : item})
       } ,
       updateCat : (item,qtyCurr) => {
           dispatch({ type : ActionType.CAT_UPDATE , payload : item , qtyCurr : qtyCurr})
       }
    }
 }
 
function mapStateToProps(state) {
    return {catState: state.catState}
}


// connect(props,dispatchprops)
// if dont wanna use props,just pass null value to connect in param at function 1
export default connect(null,mapDispatchToProps)(CatsCartPage);