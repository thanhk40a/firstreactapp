import React from 'react'
import catDetail from './cat-table-detail'

/**
 * Cat Table
 * @param {*} cats 
 */
function CatsTable(cats,removeCat,updateCat){

    return (
        <>
            <table class="table table-borderless">
                <thead style = {{padding : "10px",fontSize : "20px",textTransform : "uppercase"}}>
                    <p style={{width:"500%"}}>Our Cats</p>
                </thead>
                <tbody>
                    {   /**
                         * Check cats length
                         * @if cats.length = 0
                         * @return alert
                         * @if cats.length != 0
                         * @return cats ordered
                         */
                        cats.length == 0 
                            && 
                                <div className="alert alert-danger">You didnt choose any of cats</div>
                            ||
                                cats.map((cat,index) => 
                                    <>
                                        {catDetail(cat,cats,removeCat,updateCat)}
                                    </>
                                )       
                        }
                    </tbody>
                </table>
        </>
    )
}

/**
 * Cats Table Total
 * @param{*} cats
 */
function catsTableTotal(cats){
    // init catTotal = 0
    let catTotal = 0;
    // cat total price
    let catTotalPrice = 0;
    /**
     * Total cat qty
     * catTotal += ...cat.qty
     */
    cats.map((cat,index) => {
        catTotal += cat.qty
        catTotalPrice += (cat.qty)*(cat.cat.price)
    })
    return (
        <>
        <div className="row title-des">Cat Total</div>
        <div className="row">
            <table className="table table-borderless">
                <tbody>
                    <tr>
                        <td width="60%">Số Lượng Cats Đã Đặt : </td>
                        <td width="40%">{catTotal && catTotal || 0}</td>
                    </tr>
                    <tr>
                        <td width="60%">Chi Phí Phát Sinh (??) : </td>
                        <td width="40%">.....</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div className="row" style={{borderTop: "1px solid #8c8c8c",paddingTop: "15px"}}>
            <table className="table table-borderless">
                <tbody>
                    <tr>
                        <td width="60%"><p className="p-price-cart">Tổng Cộng : {catTotalPrice} <i className="fas fa-dollar-sign"></i></p></td>
                        <td width="40%"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        </>
    )
}

export default CatsTable
export {catsTableTotal}