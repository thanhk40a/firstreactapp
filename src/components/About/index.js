import React from 'react'
import about_medium from '../../assets/images/about_medium.png'

/**
 * About page
 * return ...
 */
const About = () => (
    <>
        <div class="container-fluid" style={{paddingTop : "2%"}}>
        <div className="row about_head"> 
                <div className="jumbotron jumbotron-fluid col-md-12">
                    <div class="container text-center">
                        <h1>こんにちは、皆さん</h1> 
                        <p>私は千羽です、よろしくお願いします。</p> 
                    </div>
                </div>
            </div>
        </div>
        <div className="row">
            <img src={about_medium} className="about_medium_image"/>
        </div>
    </>
);

export default About;
