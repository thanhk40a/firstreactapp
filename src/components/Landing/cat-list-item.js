import React from 'react'
import {Link} from 'react-router-dom'

import * as ConstRoute from '../../constanst/router'

/**
 * List item of cats
 * @param {*} cat
 * @param {*} addCat
 * @param {*} removeCat
 * @param {*} catStage 
 */
function CatListItem({cat,addCat,catState}){
    //number cat added
    var numberCatAdded = 0;

    /**
     * Map to cat stage 
     * @if cats.cat.id = cat.id
     * @return numberCatAdd = cat.qty
     */
    catState && catState.map((cats,index) => {if(cats.cat.id === cat.id){
        return numberCatAdded = cats.qty;
    }});

    /**
     * render card of cats
     */
    return (
        <div className="card">
            <Link to={ConstRoute.CatsDetail+cat.id}><img style={{width:"288px",height:"202.58px"}} className="card-img-top" src={cat.image} alt="catImage"/></Link>
            <div className="card-body">
                <div className="card-title">
                    {cat.name} 
                </div>
                <p className="card-text" style={{height : "63px"}}>
                    {cat.des}
                </p>
                <div className="row">
                    {/** check quantity of cats
                       * quantity > 0 @return quanity
                       * quantity < 0 @reuturn out of stock
                       * <div className="col-md-5"><NumericInput className="" style={{input: {width: '100px'}}} min='0' max={cat.quantity} /></div>
                    */}
                    {cat.quantity === 0 ? <div className="col-md-auto"><code>Out of stock</code></div> : <><div className="col-md-12" style={{color: "#757575",textTransform: "capitalize"}}>on stock : {cat.quantity}</div></>}
                </div>
                <div className="row">
                    <div className="col-md-12 text-center">
                        <button className="btn btn-danger" disabled = {numberCatAdded > cat.quantity-1 && true || false} onClick={()=>addCat({cat})}>Add({numberCatAdded})</button>
                    </div>
                </div>
            </div>
         </div>
    )
}

export default CatListItem