import React from "react";
import { MDBCarousel, MDBCarouselCaption, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBContainer } from "mdbreact";

/**
 * Carouse for landing page
 */
const CarouselPage = () => {
  return (
    <MDBContainer fluid style={{paddingRight: "0",paddingLeft: "0"}}>
      <MDBCarousel
        activeItem={1}
        length={3}
        showControls={true}
        showIndicators={true}
        className="z-depth-1"
      >
        <MDBCarouselInner>
          <MDBCarouselItem itemId="1">
            <MDBView>
              <img
                className="d-block w-100" style={{maxHeight: "700px"}}
                src="https://newevolutiondesigns.com/images/freebies/cat-wallpaper-preview-9.jpg"
                alt="First slide"
              />
            </MDBView>
          </MDBCarouselItem>
          <MDBCarouselItem itemId="2">
            <MDBView>
              <img
                className="d-block w-100" style={{maxHeight: "700px "}}
                src="https://newevolutiondesigns.com/images/freebies/cat-wallpaper-preview-10.jpg"
                alt="Second slide"
              />
            </MDBView>
          </MDBCarouselItem>
          <MDBCarouselItem itemId="3">
            <MDBView>
              <img
                className="d-block w-100" style={{maxHeight: "700px "}}
                src="https://newevolutiondesigns.com/images/freebies/cat-wallpaper-preview-5.jpg"
                alt="Third slide"
              />
            </MDBView>
          </MDBCarouselItem>
        </MDBCarouselInner>
      </MDBCarousel>
    </MDBContainer>
  );
}

export default CarouselPage;