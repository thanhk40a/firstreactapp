import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

import * as ConstRoute from '../../constanst/router'
import * as ActionType from '../../constanst/actiontype'
import CatListItem from './cat-list-item'
import { withFirebase } from '../Firebase';
import CarouselPage from './carousel'
import { resolve } from 'dns';
import { request } from 'https';

/**
 * Contain card of cat detail
 * @param cats
 * @param addCat
 * @param removeCat
 * @param catState
 */
function CatsCard (cats,addCat,removeCat,catState) {
   return (
      cats && cats.map((cat,index) =>
         <div className="col-md-4">
            <CatListItem cat = {cat} addCat = {addCat} removeCat = {removeCat} catState = {catState}/>
         </div>
      )
   )
}

// function promiseTest(){
//    return new Promise((resolve,reject) => {
//       let error = false;
//       if(!error){
//          resolve();
//       } else {
//          reject('Error handling');
//       }     
//    })
// }

// promiseTest().then(//function)
// .catch(....)


function setJsonCats(){
   return new Promise((resolve,reject)=>{
      setTimeout(()=>{
      },1000)
   })
}

async function jsonCats(){
   try{
      let response = await request.get('http://google.com');
   }catch(error){
      console.log(error);
   }
}

/**
 * Card of cats Page
 * @return ....
 */
class Landing extends React.Component {
   
   constructor(props){
      super(props);
      this.state = {
         loading: false,
         cats: [],
      };
   }

   componentDidMount() {
      
      setTimeout(() => {
         this.props.firebase.cats().once('value', snapshot => {
            const catsObject = snapshot.val();
            const catsList = Object.keys(catsObject).map(key => ({
               ...catsObject[key],
               uid: key,
            }));
   
            this.setState({
               cats: catsList,
               loading : true
            });
         });
       }, 1000)
       
      //  setTimeout(() => {
      //    try {
      //          const eventref = this.props.firebase.cats();
      //          const snapshot = eventref.on('value');
      //          const catsObject = snapshot.val();
      //          const catsList = Object.keys(catsObject).map(key => ({
      //             ...catsObject[key],
      //             uid: key,
      //          }));
      //          this.setState({
      //             cats: catsList,
      //             loading : true
      //          });
      //    }catch(error){
      //       console.log(error);
      //    }
      //  },1000)

       //fetch json data
      //  fetch(`https://api.coinmarketcap.com/v1/ticker/?limit=10`)
      // .then(res => res.json())
      // .then(json => this.setState({ data: json }));   
   } 
   
   //Loading screen
   loading = () => (
      <div className={this.state.loading?'landing-loaded text-center':'landing-loading text-center'}>
         <div className="spinner-grow text-muted"></div>
         <div className="spinner-grow text-primary"></div>
         <div className="spinner-grow text-success"></div>
         <div className="spinner-grow text-info"></div>
         <div className="spinner-grow text-warning"></div>
         <div className="spinner-grow text-danger"></div>
         <div className="spinner-grow text-secondary"></div>
         <div className="spinner-grow text-dark"></div>
         <div className="spinner-grow text-light"></div>
      </div>
   );

   componentWillUnmount() {
      this.props.firebase.cats().off();
   }

  render(){
     return(
      <div style={{paddingTop:"1%"}}>
      <CarouselPage/>
      <div className ="container Landing">
            {/* Loading screen */}
            {this.loading()}
            {/* Card of cats */}
            <div className="row">
               {CatsCard(this.state.cats,this.props.addCat,this.props.removeCat,this.props.catState)}
            </div>
      </div>
      </div>
     )
  }
}

/**
 * State to Props
 * @param {*} state 
 */
function mapStateToProps(state){
   return {
      catState : state.catState
   }
}

/**
 * Dispatch to Props
 * @param {*} dispatch 
 */
function mapDispatchToProps(dispatch){
   return {
      addCat : (item) => {
         dispatch({ type: ActionType.CAT_ADD , payload : item})
      },
      removeCat : (item) => {
         dispatch({ type: ActionType.CAT_REMOVE , index : item})
      }
   }
}

export default withFirebase(connect(mapStateToProps,mapDispatchToProps)(Landing));
