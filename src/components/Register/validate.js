import React from 'react'

/**
 * check if passwordone,password two is correct
 * @param {*} passwordOne 
 * @param {*} passwordTwo 
 * @return string
 */
function isValid(passwordOne,passwordTwo){
    if(passwordOne !== passwordTwo & passwordOne !== '' & passwordTwo !== '') 
        return (
            <code>Hai mật khẩu trên không trùng nhau!</code>
        )
    return(<></>)
}

/**
 * check if passwordone,password two is correct
 * @param {*} passwordOne 
 * @param {*} passwordTwo 
 * @return : @true : disable button
 *           @false : enable button
 */
function isValidBtn(passwordOne,passwordTwo){
    if(passwordOne !== passwordTwo & passwordOne !== '' & passwordTwo !== '') 
        return true;
    return false;
}

export {isValid,isValidBtn};