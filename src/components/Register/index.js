import React from 'react'
import {Link,withRouter } from 'react-router-dom'
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBIcon , MDBCardHeader} from 'mdbreact';
import { FirebaseContext } from '../Firebase';
import { withFirebase } from '../Firebase';
import { compose } from 'recompose';

import * as ConstRoute from '../../constanst/router'
import {isValid,isValidBtn} from './validate'

/**
 * Bean of register
 */
const INIT_REGIS = {
    username : '',
    email : '',
    passwordOne : '',
    passwordTwo : '',
    error : null
}

/**
 * component of register
 */
class Register extends React.Component {
    constructor(props){
        super(props)
        this.state = {...INIT_REGIS}
    }
    
    /**
     * Event on change of input
     */
    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    /**
     * Event on submit of input
     */
    onSubmit = event => {
        const { username, email, passwordOne } = this.state;
        
        this.props.firebase
          .doCreateUserWithEmailAndPassword(email, passwordOne)
          .then(authUser => {
            this.setState({ ...INIT_REGIS }); 
            alert('Đăng kí thành công! Click ok để chuyển sang trang chủ !')
            this.props.history.push(ConstRoute.Landing);
          })
          .catch(error => {
            this.setState({ error });
          });
    
        event.preventDefault();
    };
    
    render(){

        /**
         * set define of state
         */
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;

        return (
            <MDBContainer style={{paddingTop : "2%"}}>
                <MDBRow>
                    <MDBCol md="3">
                    </MDBCol>
                    <MDBCol md="6" className="card-register-col">
                    <MDBCard className="card-register"> 
                        <MDBCardBody>
                        <form onSubmit={this.onSubmit}>
                            <MDBCardHeader className="form-header warm-flame-gradient rounded">
                                <h3 className="my-3 text-center">
                                <MDBIcon icon="users" /> Đăng Kí Tài Khoản Mới
                                </h3>
                            </MDBCardHeader>
                            <div className="grey-text">
                            <MDBInput label="Nhập Tên" name="username" icon="user" value={username} onChange={this.onChange} group type="text" validate error="wrong" required success="right" />
                            <MDBInput label="Nhập Email" name="email"  icon="envelope" value={email} onChange={this.onChange} group type="email" validate error="wrong" required success="right" />
                            <MDBInput label="Nhập Mật Khẩu" minLength="6" name="passwordOne" icon="lock" group type="password" value={passwordOne} onChange={this.onChange} validate required error="wrong" success="right" />
                            <MDBInput label="Confirm Mật Khẩu" minLength="6" name="passwordTwo" value={passwordTwo} onChange={this.onChange} icon="lock" group type="password" required password />
                            </div>
                            <div className="text-center py-4 mt-3">
                            <MDBBtn  disabled={isValidBtn(passwordOne,passwordTwo)} className="btn btn-outline-purple" type="submit">
                                Đăng Kí
                                <MDBIcon far icon="paper-plane" className="ml-2" />
                            </MDBBtn>
                            </div>
                        </form>
                        {/* error from side */}
                        {isValid(passwordOne,passwordTwo)}
                        {/* error from auth */}
                        {error && <code>{error.message}</code>}
                        </MDBCardBody>
                    </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            
        );
    }
}

/**
 * Register Page
 */
const RegisterPage = () => (
    <div>
        <RegisterForm />
    </div>
)

/**
 * call firebase and round them with compose
 */
const RegisterForm = compose(withRouter,withFirebase,)(Register);

export default RegisterPage;