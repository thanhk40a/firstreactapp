import app from 'firebase/app'
import 'firebase/auth';
import 'firebase/database';

/**
 * Firebase api
 */
const config = {
    apiKey: "AIzaSyC7GIMPP2wxY7eQmV6-1dxJaOMimqF_Kxo",
    authDomain: "fir-inreact.firebaseapp.com",
    databaseURL: "https://fir-inreact.firebaseio.com",
    projectId: "fir-inreact",
    storageBucket: "fir-inreact.appspot.com",
    messagingSenderId: "395255059376",
    appId: "1:395255059376:web:2a646d8a7ca52270"
};



/**
 * Firebase class
 */
class Firebase {
    constructor() {
      app.initializeApp(config);
      // Auth firebase
      this.auth = app.auth();
      // Database firebase
      this.db = app.database();
    }
    
    // *** Auth API ***

    doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

     // *** User API ***

    cats = uid => this.db.ref(`cats/${uid}`);

    cats = () => this.db.ref('cats');

}

export default Firebase;
