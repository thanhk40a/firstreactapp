import React from 'react'
import { withFirebase } from '.';

async function FetchCatsData(props){
    try{
        const ref = await props.firebase.cats();
        const snapshot = await ref.once('value');
        const value = snapshot.val();
        console.log(snapshot.val())
        return value;
    }catch(error){
        console.log(error)
    }
    return null;
}

export default withFirebase(FetchCatsData)